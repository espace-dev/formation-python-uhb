# Formation Python UHB

Formation francophone sur l'utilisation de Python pour le calcul scientifique et 
le traitement de données (*Python for data science and analysis*)

* 20-24 novembre à l'Université Houphouët-Boigny (UHB)
* 27 novembre - 1er décembre à l'Institut National Polytechnique de Yamoussoukro (INP-HB)



## Programme

### [Jour 1](day1/jour1.md)

### [Jour 2](day2/jour2.md)

### [Jour 3](day2/jour3.md)

### [Jour 4](day2/jour4.md)

### [Jour 5](day2/jour5.md)
 

## Auteur
Benjamin Pillot



## License
[MIT licence](LICENCE)


![image](docs/espace-dev-ird-lmi-nexus.png)

