import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
import pyproj
import xarray as xr


DEFAULT_CRS = pyproj.CRS(4326)


def clip(cube, bounds=None, region=None, padding=1):
    """ Clip Xarray data array from bounds or region

    Parameters
    ----------
    cube: xarray.DataArray
    bounds: list or tuple
    region: str or geopandas.GeoDataFrame
    padding: int

    Returns
    -------

    """
    try:
        region_file = region
        region = gpd.GeoDataFrame.from_file(region_file)
    except AttributeError:
        pass

    if bounds is None:
        if region.crs != DEFAULT_CRS:
            region = region.to_crs(crs=DEFAULT_CRS)
        bounds = region.total_bounds

    lat, lon = cube[cube.dims[1]], cube[cube.dims[2]]

    # Set padding around geo boundaries while clipping
    try:
        minx = max(0, np.where(lon < bounds[0])[0][-1] - padding)
        maxx = min(np.where(lon > bounds[2])[0][0] + padding, lon.size)
        miny = max(0, np.where(lat < bounds[1])[0][-1] - padding)
        maxy = min(np.where(lat > bounds[3])[0][0] + padding, lat.size)
    except IndexError:
        raise ValueError("Requested bounds out of the Cube boundaries")

    return cube[:, miny:maxy, minx:maxx]


in_dir = '/media/benjamin/storage/DATA_DNI/FRANCE_2020/'
france = '/media/benjamin/storage/DATA_DNI/france_metro_no_islands.geojson'
region = gpd.GeoDataFrame.from_file(france)

with xr.open_mfdataset(in_dir + '*.nc', parallel=True) as data:

    dni = data.DNI

    daily_dni = data.DNI.resample(time="1D").sum()
    daily_mean_dni_per_month = daily_dni.groupby("time.month").mean()

    daily_dni_france = clip(daily_mean_dni_per_month, region=france)

    # Plot
    fig, ax = plt.subplots()
    daily_dni_france[6, :, :].plot(ax=ax)
    ax.set_title("June")
    region.to_crs(DEFAULT_CRS).plot(ax=ax, facecolor='none')

    # fig, ax = plt.subplots(2, 3, sharey=True)
    # for month_nb in range(6):
    #     daily_dni_france[month_nb, :, :].plot(ax=ax.flat[month_nb])

    plt.show()
