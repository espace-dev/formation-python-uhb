# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description."""
from pyrasta.raster import Raster

import geopandas as gpd
import numpy as np

population = Raster("/home/benjamin/Documents/PRO/PRODUITS/POPULATION_DENSITY/001_DONNEES/"
                    "COTE_D_IVOIRE/population_civ_2019-07-01_geotiff/"
                    "population_civ_2019-07-01.tif")

ci = gpd.GeoDataFrame.from_file("/home/benjamin/Documents/PRO/PRODUITS/POPULATION_DENSITY/"
                                "001_DONNEES/COTE_D_IVOIRE/geoBoundaries-CIV-ADM2.geojson")
ci = ci.to_crs(population.crs)

pop_per_region = population.zonal_stats(ci, stats=['sum'])

ci["population"] = pop_per_region["sum"]

ci.to_file("/home/benjamin/Documents/PRO/PRODUITS/POPULATION_DENSITY/001_DONNEES/COTE_D_IVOIRE"
           "/civ_test.geojson")
