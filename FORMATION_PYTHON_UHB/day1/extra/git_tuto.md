# Git commands

## Create a new repository

1. Create a project on your framagit account, such as "my_awesome_project"

2. Open a terminal and clone this remote repository on your local machine:

```shell
$ git clone https://framagit.org/your_user_name/my_awesome_project.git
```

3. Enter the folder and add a new file to your project:

```shell
$ cd my_awesome_project
$ touch README.md
```

4. Git add, commit and push this file to your remote repository:

```shell
$ git add README.md
$ git commit -m "add README"
$ git push -u origin master
```

## Push an existing folder
1. Enter your existing folder:
```shell
$ cd existing_folder
```

2. Init git and add folder to remote folder:
```shell
$ git init
$ git remote add origin https://framagit.org/your_user_name/my_awesome_project.git
$ git add .
```

3. Commit and push:
```shell
$ git commit -m "initial commit"
$ git push -u origin master
```

## Add username and password
If you do not want to always enter your username and password, you can store them. Run the following and enter both of them at prompt:

```shell
$ git config --global credential.helper store
$ git pull
```

## Git add, commit and push directly in Pycharm
Typically, once your repository exists in both local and remote environments, you may use the internal git support in Pycharm rather than command line. Let's see how we can handle that.

## Git branches
When you first create a repository, you have one default branch entitled "master". However, if various contributors work on the same project it is necessary for everyone to push to the same repository, so every contributor push a specific development branch and one admin either accept or not the change and merge the given branch with the main project.

## Git remote repositories
You can add various remote repositories if you wish. So you may have one remote on framagit, another on github, etc. You may push to both or only to one of them, etc.
