# Pycharm IDE

## Installation
Go [here](https://www.jetbrains.com/toolbox-app/) and download the Jetbrain Toolbox app. Open a terminal:
```shell
$ cd /path/to/your/downloads
$ tar -xzf jetbrains-toolbox-1.16.6319.tar.gz
```

Open the given folder and execute the `jetbrains-toolbox` bin file. Once the toolbox is ready, you can access to all Jetbrain tools, including Pycharm Community. Install the latest version.
